#define _POSIX_C_SOURCE 1
#include <signal.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <limits.h>
#include <stdlib.h>

#ifdef JUDGE_JACK
#include <jack/jack.h>
#include <unistd.h>
#endif

#ifdef JUDGE_DL
#include <dlfcn.h>
#endif

#include <fftw3.h>
#define wisdomfile "/run/shm/bitbreeder.fftw"

#include "bitbreeder.h"

static const double pi = 3.141592653589793;
#define blocksize 2048
#define overlap 4
#define BINS (blocksize / 2 + 1)
#define PARAMS 6
#define LEVELS 11

#ifdef JUDGE_DL

#define N (((1 << LEVELS) + overlap - 1) * blocksize / overlap)

struct audio {
  int length;
  float *data[2];
};

const char *dl_code = 0;

struct audio *audio(const char *name, int t) {
  struct audio *a = calloc(1, sizeof(*a));
  a->length = N;
  a->data[0] = calloc(a->length, sizeof(*a->data[0]));
  a->data[1] = calloc(a->length, sizeof(*a->data[1]));
  void *dl = dlopen(name, RTLD_NOW);
  F_t *cb;
  *(void **) (&cb) = dlsym(dl, "F");
  *(void **) (&dl_code) = dlsym(dl, "code");
  float dc[2] = { cb(t, 0), cb(t, 1) };
  float h[4][2] =
    { { dc[0], dc[1] }
    , { dc[0], dc[1] }
    , { dc[0], dc[1] }
    , { dc[0], dc[1] }
    };
  for (int i = 0; i < a->length; ++i) {
    int k = (i % 5);
    if (k == 0)
    {
      for (int c = 0; c < 2; ++c)
      {
        h[0][c] = h[1][c];
        h[1][c] = h[2][c];
        h[2][c] = h[3][c];
        h[3][c] = cb(t, c);
      }
      ++t;
    }
    for (int c = 0; c < 2; ++c)
    {
      const float p[5][4] =
        { {0.0,1.0,0.0,0.0}
        , {-0.064,0.912,0.168,-0.016}
        , {-0.072,0.696,0.424,-0.048}
        , {-0.048,0.424,0.696,-0.072}
        , {-0.016,0.168,0.912,-0.064}
        };
      float x = p[k][0] * h[0][c] + p[k][1] * h[1][c] + p[k][2] * h[2][c] + p[k][3] * h[3][c];
      dc[c] = dc[c] * 0.99 + 0.01 * x;
      a->data[c][i] = x - dc[c];
    }
  }
  dlclose(dl);
  return a;
}

#endif

struct frame
{
  float *ibuf;
  float *obuf;
  float *window;
  fftwf_plan plan;
  // computed from audio
  float loudness;
  float spectrum[BINS];
};

struct frame *frame_new(void)
{
  struct frame *f = calloc(1, sizeof(*f));
  f->ibuf = fftwf_alloc_real(blocksize);
  f->obuf = fftwf_alloc_real(blocksize);
  f->window = fftwf_alloc_real(blocksize);
  fftwf_import_wisdom_from_filename(wisdomfile);
  f->plan = fftwf_plan_r2r_1d(blocksize, f->ibuf, f->obuf, FFTW_R2HC, FFTW_DESTROY_INPUT | FFTW_EXHAUSTIVE);
  fftwf_export_wisdom_to_filename(wisdomfile);
  for (int t = 0; t < blocksize; ++t)
  {
    f->window[t] = 0.5 - 0.5 * cos(t * 2 * pi / blocksize);
  }
  return f;
}

void frame_delete(struct frame *f)
{
  fftwf_free(f->ibuf);
  fftwf_free(f->obuf);
  fftwf_free(f->window);
  fftwf_destroy_plan(f->plan);
  free(f);
}

void frame_compute(struct frame *f, const float *a)
{
  float rsqrtblocksize = 1 / sqrtf(blocksize);
  double l = 0;
  for (int t = 0; t < blocksize; ++t)
  {
    float x = a[t];
    f->ibuf[t] = f->window[t] * x;
    l += f->window[t] * x * x;
  }
  f->loudness = sqrt(l) * rsqrtblocksize;
  fftwf_execute(f->plan);
  f->spectrum[0     ] = fabsf(f->obuf[0]) * rsqrtblocksize;
  f->spectrum[BINS-1] = fabsf(f->obuf[blocksize/2]) * rsqrtblocksize;
  for (int k = 1; k < BINS-1; ++k)
  {
    float re = f->obuf[k];
    float im = f->obuf[blocksize - k];
    f->spectrum[k] = sqrtf(re * re + im * im) * rsqrtblocksize;
  }
}

enum {
  p_loudness = 0,
  p_tonality,      // min(1, (log10 m1 - 10 sum (log10 a_k) / sum 1)/60)
  p_centroid,      // m1 = sum a_k f_k / sum a_k
  p_deviation,     // m2 = sum (f_k - m1)^2 a_k / sum a_k; s^2 = m2
  p_skewness,      // m3 = sum (f_k - m1)^3 a_k / sum a_k; g1 = m3 / s^3
  p_kurtosis,      // m4 = sum (f_k - m1)^4 a_k / sum a_k; g2 = m4 / s^4
};

struct statistic {
  double s0, s1, s2;
};

void statistic(double x, struct statistic *s) {
  s->s0 = 1;
  s->s1 = x;
  s->s2 = x*x;
}

void wstatistic(double w, double x, struct statistic *s) {
  if (isnan(w)) { w = 0; }
  if (isnan(x)) { x = 0; }
  s->s0 = w;
  s->s1 = w*x;
  s->s2 = w*x*x;
}

void combine(struct statistic *x, struct statistic *y, struct statistic *r) {
  r->s0 = x->s0 + y->s0;
  r->s1 = x->s1 + y->s1;
  r->s2 = x->s2 + y->s2;
}

double mean(struct statistic *s) {
  if (! isnan(s->s1) && s->s0 > 0) {
    return s->s1 / s->s0;
  }
  return 0;
}

double stddev(struct statistic *s) {
  double d = sqrt(s->s0 * s->s2 - s->s1 * s->s1) / s->s0;
  if (! (d >= 0)) { d = 0; }
  return d;
}

struct analysis {
  struct statistic base;
  struct statistic levels[LEVELS+1];
};

void combines(int depth, struct analysis *x, struct analysis *y, struct analysis *r) {
  combine(&x->base, &y->base, &r->base);
  for (int level = 0; level < depth; ++level) {
    combine(&x->levels[level], &y->levels[level], &r->levels[level]);
  }
  statistic(stddev(&r->base), &r->levels[depth]);
}

struct analyses {
  struct analysis param[PARAMS];
};

void frame_analyse(struct frame *s, struct statistic a[PARAMS])
{
  statistic(s->loudness, &a[p_loudness]);
  {
    double s0 = 0, s1 = 0, s2 = 0, s3 = 0, s4 = 0;
    for (int k = 0; k < BINS; ++k) {
      double x = s->spectrum[k];
      double f = k * 2.0 / blocksize;
      s0 += x;
      s1 += x * f;
    }
    if (! (s0 > 0)) { s0 = 1; }
    double m1 = s1 / s0;
    for (int k = 0; k < BINS; ++k) {
      double x = s->spectrum[k];
      double f = k * 2.0 / blocksize;
      double y = f - m1;
      s2 += x * y * y;
      s3 += x * y * y * y;
      s4 += x * y * y * y * y;
    }
    double m2 = s2 / s0;
    double m3 = s3 / s0;
    double m4 = s4 / s0;
/*
    statistic(m1,                &a->param[p_centroid ].base);
    statistic(sqrt(m2),          &a->param[p_deviation].base);
    statistic(m3 / pow(m2, 1.5), &a->param[p_skewness ].base);
    statistic(m4 / pow(m2, 2),   &a->param[p_kurtosis ].base);
*/
    double l = s->loudness;
    wstatistic(l, m1,                &a[p_centroid ]);
    wstatistic(l, sqrt(m2),          &a[p_deviation]);
    wstatistic(l, m3 / pow(m2, 1.5), &a[p_skewness ]);
    wstatistic(l, m4 / pow(m2, 2),   &a[p_kurtosis ]);
  }
  {
    double s0 = 0, s1 = 0, sL = 0;
    for (int k = 0; k < BINS; ++k) {
      double x = s->spectrum[k] + 1e-6;
      s0 += 1;
      s1 += x * x;
      sL += log(x * x);
    }
    double tonality = 1 - exp(sL / s0) / (s1 / s0);
    if (! (0 < tonality)) { tonality = 0; }
    if (! (1 > tonality)) { tonality = 1; }
/*
    statistic(tonality, &a->param[p_tonality].base);
*/
    wstatistic(s->loudness, tonality, &a[p_tonality]);
  }
}

void combiness(int depth, struct analyses *x, struct analyses *y, struct analyses *r) {
  for (int param = 0; param < PARAMS; ++param) {
    combines(depth, &x->param[param], &y->param[param], &r->param[param]);
  }
}

struct analyser
{
  struct statistic stat[1 << LEVELS][PARAMS];
  int next;
};

struct analyser *analyser_new(void)
{
  return calloc(1, sizeof(struct analyser));
}

void analyser_delete(struct analyser *y)
{
  free(y);
}

void analyser_clear(struct analyser *y)
{
  memset(y, 0, sizeof(*y));
}

void analyser_next(struct analyser *y, struct statistic stat[PARAMS])
{
  memcpy(&y->stat[y->next++][0], &stat[0], sizeof(struct statistic) * PARAMS);
  y->next &= (1 >> LEVELS) - 1;
}

int analyser_analyse(struct analyser *z, int depth, int start, struct analyses *a)
{
  if (depth)
  {
    struct analyses x, y;
    start += analyser_analyse(z, depth - 1, start, &x);
    start += analyser_analyse(z, depth - 1, start, &y);
    combiness(depth - 1, &x, &y, a);
  }
  else
  {
    int i = start & ((1 << LEVELS) - 1);
    for (int p = 0; p < PARAMS; ++p)
    {
      a->param[p].base = z->stat[i][p];
    }
    start += 1;
  }
  return start;
}

struct result {
  double average, variability, granularity;
};

struct results {
  struct result result[PARAMS];
  double novelty;
  double size;
};

void results_cook(struct analyses *a, struct results *r)
{
  for (int param = 0; param < PARAMS; ++param)
  {
    r->result[param].average     = mean(&a->param[param].base);
    r->result[param].variability = mean(&a->param[param].levels[0]);
    struct statistic s, t;
    wstatistic(0, 0, &t);
    for (int level = 1; level < LEVELS; ++level)
    {
      wstatistic(level, mean(&a->param[param].levels[level]), &s);
      combine(&s, &t, &t);
    }
    r->result[param].granularity = mean(&t);
  }
}

void analyse_frame(struct analyser *y, struct frame *f)
{
  struct statistic s[PARAMS];
  frame_analyse(f, s);
  analyser_next(y, s);
}

void analyse_audio(struct analyser *y, struct frame *f, float *audio)
{
  for (int i = 0; i < 1 << LEVELS; ++i)
  {
    frame_compute(f, audio + i * blocksize / overlap);
    analyse_frame(y, f);
  }
}

void analyser_cook(struct analyser *y, struct results *r)
{
  struct analyses a;
  analyser_analyse(y, LEVELS, y->next, &a);
  results_cook(&a, r);
}

#ifdef JUDGE_JACK

#define POPULATIONS 4

struct
{
  float audio[POPULATIONS][2][blocksize];
  volatile int incb;
  jack_client_t *client;
  jack_port_t *in[POPULATIONS][2];
  struct analyser *analyser[POPULATIONS][2];
  struct frame *frame;
  int t;
} state;

int process(jack_nframes_t n, void *arg)
{
  (void) arg;
  state.incb = 1;
  float *in[POPULATIONS][2];
  for (int p = 0; p < POPULATIONS; ++p)
  {
    for (int c = 0; c < 2; ++c)
    {
      in[p][c] = jack_port_get_buffer(state.in[p][c], n);
    }
  }
  for (jack_nframes_t i = 0; i < n; ++i)
  {
    for (int p = 0; p < POPULATIONS; ++p)
    {
      for (int c = 0; c < 2; ++c)
      {
        state.audio[p][c][state.t] = in[p][c][i];
      }
    }
    if (++state.t == blocksize)
    {
      for (int p = 0; p < POPULATIONS; ++p)
      {
        for (int c = 0; c < 2; ++c)
        {
          frame_compute(state.frame, &state.audio[p][c][0]);
          analyse_frame(state.analyser[p][c], state.frame);
          for (int j = 0; j < blocksize - blocksize / overlap; ++j)
          {
            state.audio[p][c][j] = state.audio[p][c][j + blocksize / overlap];
          }
        }
      }
      state.t -= blocksize / overlap;
    }
  }
  state.incb = 0;
  return 0;
}

volatile int running = 1;

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  srand(time(0));
  memset(&state, 0, sizeof(state));
  for (int p = 0; p < POPULATIONS; ++p)
  {
    for (int c = 0; c < 2; ++c)
    {
      state.analyser[p][c] = analyser_new();
    }
  }
  state.frame = frame_new();
  state.client = jack_client_open("bitbreeder-judge", JackNoStartServer, 0);
  if (! state.client)
  {
    return 1;
  }
  for (int p = 0; p < POPULATIONS; ++p)
  {
    for (int c = 0; c < 2; ++c)
    {
      char port_name[64];
      snprintf(port_name, sizeof(port_name), "input_%d.%d", p, c + 1);
      state.in[p][c] = jack_port_register(state.client, port_name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    }
  }
  jack_set_process_callback(state.client, process, 0);
  if (jack_activate(state.client))
  {
    return 1;
  }
  int connected = 1;
  for (int p = 0; p < POPULATIONS; ++p)
  {
    for (int c = 0; c < 2; ++c)
    {
      char port_name[64];
      snprintf(port_name, sizeof(port_name), "bitbreeder-%d:output_%d", p, c + 1);
      if (jack_connect(state.client, port_name, jack_port_name(state.in[p][c])))
      {
        connected = 0;
      }
    }
  }
  if (connected)
  {
    fprintf(stderr, "bitbreeder-judge connected\n");
  }
  else
  {
    fprintf(stderr, "bitbreeder-judge could not connect\n");
  }
  struct {
    double s0, s1, s2, now, slow, slow_speed;
  } S[POPULATIONS][PARAMS * 3];
  memset(S, 0, sizeof(S));
  for (int p = 0; p < POPULATIONS; ++p)
  {
    for (int k = 0; k < 3 * PARAMS; ++k)
    {
      S[p][k].slow_speed = 0.001 * pow(10, rand() / (double) RAND_MAX);
    }
  }
  int n = 0;
  while (running)
  {
    sleep(1);
    for (int p = 0; p < POPULATIONS; ++p)
    {
      struct results rl, rr;
      analyser_cook(state.analyser[p][0], &rl);
      analyser_cook(state.analyser[p][1], &rr);
      int q = 0;
      for (int param = 0; param < PARAMS; ++param)
      {
        S[p][q++].now = (rl.result[param].average     + rr.result[param].average    ) / 2;
        S[p][q++].now = (rl.result[param].variability + rr.result[param].variability) / 2;
        S[p][q++].now = (rl.result[param].granularity + rr.result[param].granularity) / 2;
      }
      for (int k = 0; k < 3 * PARAMS; ++k)
      {
        S[p][k].s0 += 1;
        S[p][k].s1 += S[p][k].now;
        S[p][k].s2 += S[p][k].now * S[p][k].now;
        if (n == 0)
        {
          S[p][k].slow = S[p][k].now;
        }
        else
        {
          S[p][k].slow += (S[p][k].now - S[p][k].slow) * S[p][k].slow_speed;
        }
        if (n > 60)
        {
          S[p][k].s0 -= 1;
          S[p][k].s1 -= S[p][k].slow;
          S[p][k].s2 -= S[p][k].slow * S[p][k].slow;
        }
        // fprintf(stderr, "%d %d %.7f %.7f\n", p, k, S[p][k].now, S[p][k].slow);
      }
    }
    if (n == 60)
    {
      for (int p = 0; p < POPULATIONS; ++p)
      {
        printf("W %d 0 0.3\n", p);
        printf("T %d 0 3.0\n", p);
        for (int k = 1; k < 3 * PARAMS; ++k)
        {
          printf("W %d %d 0.1\n", p, k);
        }
      }
    }
    else if (n > 60)
    {
      for (int k = 1; k < 3 * PARAMS; ++k)
      {
        double variance[POPULATIONS];
        for (int p = 0; p < POPULATIONS; ++p)
        {
          variance[p] = (S[p][k].s2 * S[p][k].s0 - S[p][k].s1 * S[p][k].s1) / (S[p][k].s0 * S[p][k].s0);
        }
        for (int p = 0; p < POPULATIONS; ++p)
        {
          double target = 0, delta;
          // move further away from own past
          delta = ((S[p][k].now - S[p][k].slow) / sqrt(variance[p]));
          if (isnan(delta))
          {
            delta = 0;
          }
          target += delta;
          for (int q = 0; q < POPULATIONS; ++q)
          {
            if (q != p)
            {
              // move away from other population
              delta = ((S[p][k].slow - S[q][k].slow) / sqrt(variance[p] + variance[q]));
              if (isnan(delta))
              {
                delta = 0;
              }
              target += delta;
            }
          }
          target = 5 * tanh(target / POPULATIONS);
          printf("T %d %d %.7f\n", p, k, target);
          printf("W %d %d %.7f\n", p, k, fabs(target) / 5);
        }
      }
    }
    fflush(stdout);
    ++n;
  }
}

#endif

#ifdef JUDGE_DL

const char *name = 0;
int t0 = 0;

void fpe_handler(int s) {
  (void) s;
  fprintf(stderr, "SIGFPE %s %d %s\n", name, t0, dl_code);
  exit(1);
}

int main(int argc, char **argv) {
  (void) argc;
  name = argv[1];
  t0 = time(0) * (48000 / 5);
  struct sigaction act, old;
  act.sa_handler = fpe_handler;
  sigemptyset(&act.sa_mask);
  sigaction(SIGFPE, &act, &old);
  struct results rl, rr;
  struct audio *a = audio(argv[1], t0);
  struct frame *f = frame_new();
  struct analyser *y = analyser_new();
  analyse_audio(y, f, a->data[0]);
  analyser_cook(y, &rl);
  analyser_clear(y);
  analyse_audio(y, f, a->data[1]);
  analyser_cook(y, &rr);
  analyser_delete(y);
  frame_delete(f);
  // FIXME compute stereo width metrics
  struct results r;
  for (int param = 0; param < PARAMS; ++param)
  {
    r.result[param].average     = (rl.result[param].average     + rr.result[param].average    ) / 2;
    r.result[param].variability = (rl.result[param].variability + rr.result[param].variability) / 2;
    r.result[param].granularity = (rl.result[param].granularity + rr.result[param].granularity) / 2;
  }
  r.novelty = atoi(argv[2]);
  r.size = atoi(argv[3]);
  fwrite(&r, sizeof(r), 1, stdout);
  sigaction(SIGFPE, &old, 0);
  return 0;
}

#endif
