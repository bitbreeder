#include <limits.h>

#include "bitbreeder.h"

static inline int safe_div(int a, int b) { if ((b == 0) || (a == INT_MIN && b == -1)) { return 0; } else { return a / b; } }
static inline int safe_mod(int a, int b) { if ((b == 0) || (a == INT_MIN && b == -1)) { return 0; } else { return a % b; } }
static inline int safe_mul(int a, int b) { return (int)(((long long)(a)) * ((long long)(b))); }
static inline int safe_add(int a, int b) { return (int)(((long long)(a)) + ((long long)(b))); }
static inline int safe_sub(int a, int b) { return (int)(((long long)(a)) - ((long long)(b))); }
static inline int safe_shl(int a, int b) { return (int)(((long long)(a)) << (b & 31)); }
static inline int safe_shr(int a, int b) { return (int)(((long long)(a)) >> (b & 31)); }

const char code[] = CODE;
float F(int t, int c) { return (((T)&255)-128)/256.0f; }
