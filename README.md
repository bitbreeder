# BitBreeder

BitBreeder evolves noisy expressions.

## Requirements

### Build

BitBreeder is written in Haskell and C, user interface in HTML and JavaScript.

You need ghc and cabal-install, see bitbreeder.cabal for dependencies.

You need gcc and these libraries:

    m dl jack fftw3f

You need make to build.

### Runtime

You need JACK-compatible audio server (running at 48000Hz),
and gcc (BitBreeder generates source code and compiles it).

You need a web browser supporting JavaScript and WebSockets,
open [bitbreeder.html](bitbreeder.html) to control your local BitBreeder.

## Usage

First configure your JACK daemon (maybe with qjackctl).
Build:

    make
    cabal install --overwrite-policy=always

Run (example for 16-thread desktop CPU, adjust to your system):

    bitbreeder +RTS -N12

To exit, Ctrl-C in the terminal.

Control:

    firefox bitbreeder.html

## Controls

The web interface has a bunch of sliders.  Each row corresponds to
an audio descriptor, the left slider is the normalized target value and the
right slider is the weighting. Above is displays the currently
sounding expression, which is the fittest expression matching the current
state.  There are four populations, each with their own fitness target/weights.
BitBreeder cross-breeds and mutates the expressions from each population.

Targets are normalized: the center of the slider range is the mean of the
population, and the extremes of the slider range are +/- a few standard
deviations of the population.   Weighting is from 0 at the left increasing
to the right, changing a target will have no effect when its weight is 0.
When all weights are zero each newly generated expression is deemed the
fittest.

## Implementation

BitBreeder consists of a few programs:
control logic and WebSocket server (bitbreeder),
user interface WebSocket client (bitbreeder.html),
live audio generation (bitbreeder-audio),
expression audio analysis (bitbreeder-judge),
and visualisation of the expression (bitbreeder-video, currently disabled).

### bitbreeder

FIXME cross-breeding, mutation, populations, compilation

### bitbreeder-judge

The judge loads an .so containing the compiled expression, and generates
a couple of minutes of audio with it.  The main loop is a tree structure:

    recursion depth
    :    /
    2   /\
       /  \
    1 /\  /\
    0/\/\/\/\/
     01234567.. audio frames

Each audio frame is an FFT spectrum and RMS volume for that block (which
are windowed and overlapped).

At the lowest level of the tree the basic instantaneous descriptors are
calculated (tonality, spectral centroid, etc...), each stored as a
statistic (weight, weight * value, weight * value^2) with the weight
usually based on the RMS volume.

Each subsequent level combines statistics from all the previous levels -
each node (for each basic descriptor) combines pairwise two lists of
statistics (ordered by level) and adds a new statistic as the mean of
the level below it.

Example (assuming weight is 1):

```
-> input descriptor sequence
  7
  4
  5
  1
-> base level
  1 7 49
  1 4 16
  1 5 25
  1 1  1
-> next level
  2 11 98    1 5.5 30.25
  2  6 36    1 3    9
-> top level
  4 17 134   2 8   39.25    1 4 16
-> final results
  mean(0) = 17/4 = 4.25
  stddev(0) = sqrt(4 * 134 - 17^2) / 4 = 3.929
  stddev(1) = sqrt(2 * 39.25 - 8^2) / 2 = 1.936
  stddev(2) = sqrt(1 * 16 - 4^2) / 1 = 0
  granularity = centroid of stddev(level)
    = (0 * 3.929 + 1 * 1.936 + 2 * 0) / (0 + 1 + 2)
    = 0.64549
-> output
  mean(0), stddev(0), granularity
```

### bitbreeder-audio

The main loop watches stdin for names of an .so containing a compiled
expression.  For each line, it loads the .so and swaps the JACK process
callback to the new expression (taking care not to unload code that is
currently running).

FIXME sample rate conversion, DC offset removal

### bitbreeder-video

The main loop watches stdin for expressions in Haskell's Show syntax.
For each line, it parses the expression, lays it out as a tree, and
displays it.

FIXME glyph map, textureQueryLod

## Legal

Copyright (C) 2013,2020,2024  Claude Heiland-Allen <claude@mathr.co.uk>
License: AGPLv3 only
Warranty: NONE

-- 
<https://mathr.co.uk>
