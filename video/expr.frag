#version 330 compatibility
#extension GL_ARB_texture_rectangle : enable

uniform sampler2D     glyphs;
uniform sampler2DRect expression;

const float yf  = 4.0/6.0;
const float ylo = 1.0/6.0;
const float yhi = 5.0/6.0;

float myTextureQueryLod(sampler2D tex, vec2 tc) {
  //return textureQueryLod(tex, tc).y;
  return max(0.0, 10.0 + log2(max(length(dFdx(tc)), length(dFdy(tc)))));
}

void main() {
  vec2 tc = gl_TexCoord[0].xy * vec2(0.125, 0.25 * yf);
  float lod = myTextureQueryLod(glyphs, tc);
  vec2 gc = floor(gl_TexCoord[0].xy * vec2(1.0, yf));
  vec3 glyph = texture2DRect(expression, gc).xyz;
  vec2 glyphCoord = glyph.xy;
  vec2 subCoord = fract(gl_TexCoord[0].xy * vec2(1.0, yf));
  if (subCoord.y < ylo) {
    glyphCoord = vec2(0.125, 0.75);
  } else if (yhi < subCoord.y) {
    glyphCoord = vec2(0.125, 0.75);
    subCoord.y += ylo - yhi;
  }
  subCoord.y -= ylo;
  subCoord *= vec2(0.125, 0.25 / yf);
  vec3 glyphRGB = textureLod(glyphs, glyphCoord + subCoord, lod).xyz;
  gl_FragColor = vec4(glyphRGB, 1.0);
}
