#!/bin/bash
# sudo cpufreq-set -c 0 -g performance
# sudo cpufreq-set -c 1 -g performance
ulimit -s unlimited
rm -rf o
rm -f v
make
SESSION="bitbreeder-$(date -u +%F-%H%M%S)"
mkdir -p o a "a/${SESSION}"
ln -s "a/${SESSION}/" v
ecasound -q -G:jack,record -f:f32,1,48000 -i:jack -o "a/${SESSION}.wav" &
sleep 5
time ./bitbreeder +RTS -N 2>"a/${SESSION}.err" >"a/${SESSION}.out"
sleep 5
kill %ecasound
echo "./encode.sh \"a/${SESSION}\""
