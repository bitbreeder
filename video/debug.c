#include <stdio.h>
#include <dlfcn.h>

int main(int argc, char **argv) {
  void *dl = dlopen(argv[1], RTLD_NOW);
  const char *code = dlsym(dl, "code");
  printf("%s\n", code);
  return 0;
}
