#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sndfile.h>
#include <fftw3.h>

#define wisdomfile "/run/shm/bitbreeder.fftw"

static const double pi = 3.141592653589793;
static const double sr = 8192.0;
static const int bs = 2048;
static const int ol = 4;

struct audio {
  int channels;
  int frames;
  float *data;
};

static struct audio *audio_load(const char *filename) {
  struct audio *audio = calloc(1, sizeof(*audio));
  if (! audio) {
    return 0;
  }
  SF_INFO info; memset(&info, 0, sizeof(info));
  SNDFILE *in = sf_open(filename, SFM_READ, &info);
  if (! in) {
    free(audio);
    return 0;
  }
  audio->channels = info.channels;
  audio->frames = info.frames;
  audio->data = calloc(audio->channels * audio->frames, sizeof(*audio->data));
  if (! audio->data) {
    sf_close(in);
    free(audio);
    return 0;
  }
  sf_readf_float(in, audio->data, audio->frames);
  sf_close(in);
  return audio;
}

struct fft {
  float *window;
  float *in;
  float *out;
  float *ibuf;
  float *obuf;
  fftwf_plan plan;
};

static struct fft *fft_init() {
  struct fft *fft = calloc(1, sizeof(*fft));
  if (! fft) {
    return 0;
  }
  fft->window = calloc(bs, sizeof(*fft->window));
  fft->in = calloc(bs, sizeof(*fft->in));
  fft->out = calloc(bs, sizeof(*fft->out));
  fft->ibuf = fftwf_alloc_real(bs);
  fft->obuf = fftwf_alloc_real(bs);
  for (int t = 0; t < bs; ++t) {
    fft->window[t] = 0.5 - 0.5 * cos(t * 2 * pi / bs);
  }
  fftwf_import_wisdom_from_filename(wisdomfile);
  fft->plan = fftwf_plan_r2r_1d(bs, fft->ibuf, fft->obuf, FFTW_R2HC, FFTW_DESTROY_INPUT | FFTW_EXHAUSTIVE);
  fftwf_export_wisdom_to_filename(wisdomfile);
  return fft;
}

static void fft_compute(struct fft *fft) {
  for (int k = 0; k < bs; ++k) {
    fft->ibuf[k] = fft->window[k] * fft->in[k];
  }
  fftwf_execute(fft->plan);
  fft->out[0] = fft->obuf[0] * fft->obuf[0] / sqrtf(bs);
  fft->out[bs/2] = fft->obuf[bs/2] * fft->obuf[bs/2] / sqrtf(bs);
  for (int k = 1; k < bs/2; ++k) {
    float re = fft->obuf[k];
    float im = fft->obuf[bs - k]; 
    fft->out[k] = sqrtf(re * re + im * im) / sqrtf(bs);
  }
}

struct plane {
  int width;
  int height;
  float *data;
};

struct planes {
  int count;
  struct plane *plane;
};

static void planes_copy(float *src, int count, struct planes *planes, int p, int x) {
  for (int i = 0; i < count; ++i) {
    planes->plane[p].data[planes->plane[p].width * i + x] = src[i];
  }
}

static void audio_copy(float *dst, int count, struct audio *audio, int t0, int c) {
  int t = t0;
  for (int i = 0; i < count; ++i) {
    if (0 <= t && t < audio->frames) {
      dst[i] = audio->data[t * audio->channels + c];
    } else {
      dst[i] = 0;
    }
    ++t;
  }
}

static struct planes *planes_init(struct audio *audio, struct fft *fft, int count) {
  struct planes *planes = calloc(1, sizeof(*planes)); // FIXME cleanup
  planes->count = count;
  planes->plane = calloc(planes->count, sizeof(*planes->plane)); // FIXME cleanup
  int i = 0;
//  for (int i = 0; i < planes->count; ++i) {
//    fprintf(stderr, "%d\n", i);
    double frames = 2 * bs + audio->frames;
    planes->plane[i].width = ceil(frames * ol / bs);
    planes->plane[i].height = bs;
    planes->plane[i].data = calloc(planes->plane[i].width * planes->plane[i].height, sizeof(*planes->plane[i].data)); // FIXME cleanup
    int x = 0;
    for (int t = -bs; t < bs + audio->frames; t += bs / ol) {
      for (int c = 0; c < audio->channels; ++c) {
        audio_copy(fft->in, bs, audio, t, c);
        fft_compute(fft);
        planes_copy(fft->out, bs, planes, i, x);
      }
      ++x;
    }
//    struct audio *audio2 = audio_downsample(audio);
//    free(audio);
//    audio = audio2;
//  }
  return planes;
}

struct image {
  int width;
  int height;
  unsigned char *data;
};

static struct image *image_init(int frames) {
  struct image *image = calloc(1, sizeof(*image));
  if (! image) {
    return 0;
  }
  image->height = 128;
  double dframes = frames;
  image->width = ceil(dframes * ol / bs);
  image->data = calloc(1, image->width * image->height);
  if (! image->data) {
    free(image);
    return 0;
  }
  return image;
}

static void image_write(struct image *image, const char *filename) {
  FILE *out = fopen(filename, "wb");
  if (out) {
    fprintf(out, "P5\n%d %d\n255\n", image->width, image->height);
    fwrite(image->data, image->width * image->height, 1, out);
    fclose(out);
  }
}

static double planes_lookup(struct planes *planes, double f0, double f1, double t0) {
  double y0 = f0 / sr * planes->plane[0].height;
  double y1 = f1 / sr * planes->plane[0].height;
  int v0 = floor(y0);
  int v1 = ceil(y1);
  double z = 0;
  double t = t0;
  double x = ol * t / bs;
  int u = floor(x);
  int k = 0;
  for (int v = v0; v < v1; ++v) {
    if (0 <= u && u < planes->plane[0].width && 0 <= v && v < planes->plane[0].height) {
      z += planes->plane[0].data[planes->plane[0].width * v + u];
      k += 1;
    }
  }
  if (k == 0) { k = 1; }
  return z / k;
}

static void compute(struct planes *planes, struct image *image) {
  unsigned char *data = image->data;
  for (int y = 0; y < image->height; ++y) {
    double f0 = (pow(2, (y     - image->height) * 1.0 / image->height) - 0.5) * sr;
    double f1 = (pow(2, (y + 1 - image->height) * 1.0 / image->height) - 0.5) * sr;
    for (int x = 0; x < image->width; ++x) {
      double t = (x * bs) / ol + (ol - 1) * bs/ol;
      double v = planes_lookup(planes, f0, f1, t);
      unsigned char g = fmin(fmax(255 * v, 0), 255);
      *data++ = g;
    }
  }
}

int main(int argc, char **argv) {
  if (argc < 3) {
    return 1;
  }
  int retval = 1;
  struct audio *audio = audio_load(argv[1]);
  if (audio) {
    struct fft *fft = fft_init();
    if (fft) {
      int frames = audio->frames;
      struct planes *planes = planes_init(audio, fft, 1);
      if (planes) {
        struct image *image = image_init(frames);
        if (image) {
          compute(planes, image);
          image_write(image, argv[2]);
          retval = 0;
//          image_free(image);
        }
//        planes_free(planes);
      }
//      fft_free(fft);
    }
  }
  return retval;
}
