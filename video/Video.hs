module Video (setupGL, draw, pngFilename, captureToPNG) where

import Data.Maybe (mapMaybe)
import Data.List (intercalate, transpose)
import Foreign (allocaBytes, nullPtr, peek, peekArray, plusPtr, with, withArray)
import Foreign.C (peekCStringLen, withCString)
import System.IO (hPutStrLn, stderr, hGetBuf, withBinaryFile, IOMode(ReadMode))
import Graphics.Rendering.Cairo (Format(FormatRGB24), formatStrideForWidth, surfaceWriteToPNG, withImageSurfaceForData)
import Graphics.GL

import Config (videoW, videoH)
import Expression

type Glyph  = Char

data Layout = Layout [Glyph] (Int, Int) [Layout]

layout :: E -> Layout
layout X = Layout "t" (1, 1) []
layout Y = Layout "c" (1, 1) []
layout (I i) = Layout s (length s, 1) [] where s = show i
layout (U u e) = Layout s (w `max` length s, h + 1) [l]
  where
    s = glyphsU u
    l@(Layout _ (w, h) _) = layout e
layout (B b e f) = Layout s (ew + fw + 1, (eh `max` fh) + 1) [el, fl]
  where
    s = glyphsB b
    el@(Layout _ (ew, eh) _) = layout e
    fl@(Layout _ (fw, fh) _) = layout f
layout (T e f g) = Layout "?:" (ew + fw + gw + 2, (eh `max` fh `max` gh) + 1) [el, fl, gl]
  where
    el@(Layout _ (ew, eh) _) = layout e
    fl@(Layout _ (fw, fh) _) = layout f
    gl@(Layout _ (gw, gh) _) = layout g

type Position = (Float, Float)
type Edge = (Position, Position)

layoutEdges :: Layout -> (Position, [Edge])
layoutEdges (Layout _ (w, _) []) = ((fromIntegral w / 2, 0.5), [])
layoutEdges (Layout gs _ [l@(Layout _ _ _)]) = (t, (t, (tx, ty + 1)) : map translate es)
  where
    t = (fromIntegral (length gs) / 2, 0.5)
    ((tx, ty), es) = layoutEdges l
    translate ((x0, y0), (x1, y1)) = ((x0, y0 + 1), (x1, y1 + 1))
layoutEdges (Layout gs _ [l@(Layout _ (lw, _) _), r]) = (t, (t, (lx, ly + 1)) : (t, (rx + fromIntegral lw + 1, ry + 1)) : map translateL ls ++ map translateR rs)
  where
    t = (fromIntegral lw + fromIntegral (length gs) / 2, 0.5)
    ((lx, ly), ls) = layoutEdges l
    ((rx, ry), rs) = layoutEdges r
    translateL ((x0, y0), (x1, y1)) = ((x0, y0 + 1), (x1, y1 + 1))
    translateR ((x0, y0), (x1, y1)) = ((x0 + fromIntegral lw + 1, y0 + 1), (x1 + fromIntegral lw + 1, y1 + 1))
layoutEdges (Layout gs _ [l@(Layout _ (lw, _) _), m@(Layout _ (mw, _) _), r]) =
  (t, (t, (lx, ly + 1)) :
      (t, (mx + fromIntegral lw + 1, my + 1)) :
      (t, (rx + fromIntegral lw + 1 + fromIntegral mw + 1, ry + 1)) :
      map translateL ls ++ map translateM ms ++ map translateR rs)
  where
    t = (fromIntegral lw + fromIntegral (length gs) / 2, 0.5)
    ((lx, ly), ls) = layoutEdges l
    ((mx, my), ms) = layoutEdges m
    ((rx, ry), rs) = layoutEdges r
    translateL ((x0, y0), (x1, y1)) = ((x0, y0 + 1), (x1, y1 + 1))
    translateM ((x0, y0), (x1, y1)) = ((x0 + fromIntegral lw + 1, y0 + 1), (x1 + fromIntegral lw + 1, y1 + 1))
    translateR ((x0, y0), (x1, y1)) = ((x0 + fromIntegral lw + 1 + fromIntegral mw + 1, y0 + 1), (x1 + fromIntegral lw + 1 + fromIntegral mw + 1, y1 + 1))

glyphsU :: U -> [Glyph]
glyphsU Neg  = "-"
glyphsU LNot = "!"
glyphsU BNot = "~"

glyphsB :: B -> [Glyph]
glyphsB Add = "+"
glyphsB Sub = "-"
glyphsB Mul = "*"
glyphsB Div = "/"
glyphsB Mod = "%"
glyphsB BAnd = "&"
glyphsB LAnd = "&&"
glyphsB BOr = "|"
glyphsB LOr = "||"
glyphsB XOr = "^"
glyphsB ShL = "<<"
glyphsB ShR = ">>"
glyphsB Lt = "<"
glyphsB Gt = ">"

pretty :: Layout -> [[(Glyph, Float)]]
pretty l@(Layout _ (w, h) _) = map (([space] ++).(++ [space])) $ [replicate w space] ++ fst (pretty' h l [0..]) ++ [replicate w space] where space = (' ', -1)
pretty' :: Int -> Layout -> [Float] -> ([[(Glyph, Float)]], [Float])
pretty' _ _ [] = error "pretty'"
pretty' h (Layout s (w, _) ls) (c:cs) = (take h (take w (replicate x space ++ (zip s (repeat c)) ++ repeat space) : combine [space] gs ++ repeat (replicate w space)), cs')
  where
    (gs, cs') = maps (pretty' (h - 1)) cs ls
    space = (' ', c)
    x = case ls of
          (Layout _ (w', _) _):_:_ -> w'
          _ -> 0

maps :: (Layout -> [Float] -> ([[(Glyph, Float)]], [Float])) -> [Float] -> [Layout] -> ([[[(Glyph, Float)]]], [Float])
maps _ cs [] = ([], cs)
maps p cs (l:ls) =
  let (g, cs') = p l cs
      (gs, cs'') = maps p cs' ls
  in  (g:gs, cs'')

combine :: [(Glyph, Float)] -> [[[(Glyph, Float)]]] -> [[(Glyph, Float)]]
combine space = map (intercalate space) . transpose

glyphMap :: [(Glyph, [Float])]
glyphMap = [ (g, [x/8, y/4]) | (gs, y) <- ["01234567", "89-~!+*/", "%^&|<>?:", "tc "] `zip` [0..], (g, x) <- gs `zip` [0..] ]

uploadGlyphs :: [[(Glyph, Float)]] -> IO (Int, Int)
uploadGlyphs gss@(gs:_) = do
  let w = length gs
      h = length gss
      xyzs = concat . mapMaybe (\(g, z) -> fmap (++[z]) $ g `lookup` glyphMap) . concat $ gss
  withArray xyzs $ glTexImage2D GL_TEXTURE_RECTANGLE 0 (fromIntegral GL_RGB32F) (fromIntegral w) (fromIntegral h) 0 GL_RGB GL_FLOAT
  return (w, h)
uploadGlyphs _ = return (0, 0)

toTexture :: Layout -> IO (Int, Int)
toTexture = uploadGlyphs . pretty

setupGL :: IO (GLuint, GLuint)
setupGL = do
  [t0, t1] <- withArray [0,0] $ \p -> glGenTextures 2 p >> peekArray 2 p
  glActiveTexture GL_TEXTURE1
  glBindTexture GL_TEXTURE_2D t1
  let width = 1024
      height = 512
      bytes = width * height * 3
  withBinaryFile "glyphs.raw" ReadMode $ \h -> allocaBytes bytes $ \p -> do
    _ <- hGetBuf h p bytes
    glTexImage2D GL_TEXTURE_2D 0 (fromIntegral GL_RGB) (fromIntegral width) (fromIntegral height) 0 GL_RGB GL_UNSIGNED_BYTE p
  glGenerateMipmap GL_TEXTURE_2D
  glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER (fromIntegral GL_LINEAR_MIPMAP_LINEAR)
  glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER (fromIntegral GL_LINEAR)
  glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_S (fromIntegral GL_CLAMP_TO_EDGE)
  glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_T (fromIntegral GL_CLAMP_TO_EDGE)
  glActiveTexture GL_TEXTURE0
  glBindTexture GL_TEXTURE_RECTANGLE t0
  glTexParameteri GL_TEXTURE_RECTANGLE GL_TEXTURE_MIN_FILTER (fromIntegral GL_NEAREST)
  glTexParameteri GL_TEXTURE_RECTANGLE GL_TEXTURE_MAG_FILTER (fromIntegral GL_NEAREST)
  glTexParameteri GL_TEXTURE_RECTANGLE GL_TEXTURE_WRAP_S (fromIntegral GL_CLAMP_TO_EDGE)
  glTexParameteri GL_TEXTURE_RECTANGLE GL_TEXTURE_WRAP_T (fromIntegral GL_CLAMP_TO_EDGE)
  prog <- glCreateProgram
  frag <- glCreateShader GL_FRAGMENT_SHADER
  src <- readFile "expr.frag"
  withCString src $ \p -> with p $ \pp -> glShaderSource frag 1 pp nullPtr
  glCompileShader frag
  hPutStrLn stderr =<< shaderLog frag
  glAttachShader prog frag
  glLinkProgram prog
  hPutStrLn stderr =<< programLog prog
  glUseProgram prog
  withCString "glyphs" $ \s -> glGetUniformLocation prog s >>= \l -> glUniform1i l 1
  withCString "expression" $ \s -> glGetUniformLocation prog s >>= \l -> glUniform1i l 0
  prog2 <- glCreateProgram
  frag2 <- glCreateShader GL_FRAGMENT_SHADER
  src2 <- readFile "stroke.frag"
  withCString src2 $ \p -> with p $ \pp -> glShaderSource frag2 1 pp nullPtr
  glCompileShader frag2
  hPutStrLn stderr =<< shaderLog frag2
  glAttachShader prog2 frag2
  glLinkProgram prog2
  hPutStrLn stderr =<< programLog prog2
  return (prog, prog2)

draw :: (GLuint, GLuint) -> E -> IO ()
draw (prog, prog2) e = do
  let l = layout e
      (_, es) = layoutEdges l
  (w, h') <- toTexture l
  let h  = 1.5 * fromIntegral h'
      w0 = 32 * h / 9
      h0 = 9 * fromIntegral w / 32
      x0 = negate (w0 - fromIntegral w) / 2
      x1 = x0 + w0
      y0 = negate (h0 - h) / 2
      y1 = y0 + h0
  if 9 * fromIntegral w > 32 * h
    then do
      glDisable GL_BLEND
      glUseProgram prog
      quad (0, fromIntegral w) (y1, y0)
      glEnable GL_BLEND
      glBlendFunc GL_DST_COLOR GL_ZERO -- multiplicative
      glUseProgram prog2
      glBegin GL_QUADS
      mapM_ (edge (0, fromIntegral w) (y1, y0)) es
      glEnd
    else do
      glDisable GL_BLEND
      glUseProgram prog
      quad (x0, x1) (h*1.5, 0)
      glEnable GL_BLEND
      glBlendFunc GL_DST_COLOR GL_ZERO -- multiplicative
      glUseProgram prog2
      glBegin GL_QUADS
      mapM_ (edge (x0, x1) (h*1.5, 0)) es
      glEnd
  where
    v vx vy tx ty = glTexCoord2f tx ty >> glVertex2f vx vy
    quad (x0, x1) (y0, y1) = do
      glBegin GL_QUADS
      v (-1) (-1) x0 y0
      v (-1)   1  x0 y1
      v   1    1  x1 y1
      v   1  (-1) x1 y0
      glEnd
    edge (x0, x1) (y0, y1) ((u0, v0), (u1, v1)) = do
      let p0 = ( u0+1      - x0) / (x1 - x0) * 2 - 1
          q0 = ((v0+1)*1.5 - y0) / (y1 - y0) * 2 - 1
          p1 = ( u1+1      - x0) / (x1 - x0) * 2 - 1
          q1 = ((v1+1)*1.5 - y0) / (y1 - y0) * 2 - 1
          dp' = p1 - p0
          dq' = q1 - q0
          d = sqrt (dp' * dp' + dq' * dq') * (x1 - x0)
          dp = 2 * 0.09 * ( dq') / d
          dq = 2 * 0.32 * (-dp') / d
      v (p0 - dp) (q0 - dq) 0 0
      v (p0 + dp) (q0 + dq) 0 1
      v (p1 + dp) (q1 + dq) 1 1
      v (p1 - dp) (q1 - dq) 1 0

programLog :: GLuint -> IO String
programLog prog = do
  l <- with 0 $ \p -> glGetProgramiv prog GL_INFO_LOG_LENGTH p >> peek p
  allocaBytes (fromIntegral l) $ \p -> with 0 $ \q -> do
    glGetProgramInfoLog prog (fromIntegral l) q p
    m <- peek q
    peekCStringLen (p, fromIntegral m)

shaderLog :: GLuint -> IO String
shaderLog prog = do
  l <- with 0 $ \p -> glGetShaderiv prog GL_INFO_LOG_LENGTH p >> peek p
  allocaBytes (fromIntegral l) $ \p -> with 0 $ \q -> do
    glGetShaderInfoLog prog (fromIntegral l) q p
    m <- peek q
    peekCStringLen (p, fromIntegral m)

pngFilename :: Int -> String
pngFilename n = "./v/" ++ (reverse . take 8 . (++ repeat '0') . reverse . show) n ++ ".png"

captureToPNG :: String -> IO ()
captureToPNG f = do
  let stride = formatStrideForWidth FormatRGB24 videoW
  allocaBytes (videoH * stride) $ \p -> do
    glPixelStorei GL_PACK_ROW_LENGTH (fromIntegral $ stride `div` 4)
    glReadPixels 0 0 (fromIntegral videoW) (fromIntegral videoH) GL_BGRA GL_UNSIGNED_BYTE p
    glPixelStorei GL_PACK_ROW_LENGTH 0
    let q = p `plusPtr` ((videoH - 1) * stride)
    withImageSurfaceForData q FormatRGB24 videoW videoH (-stride) $ \s -> do
      surfaceWriteToPNG s f
