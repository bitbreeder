module Config where

videoW, videoH, videoX, videoY :: Int

videoW = 2560
videoH = 720

videoX = screen1W - videoW
videoY = 0

{-
videoW = screen2W
videoH = (9 * videoW) `div` 16

videoX = screen1W
videoY = (screen2H - videoH) `div` 2
-}

screen1W, screen1H, screen2W, screen2H :: Int

screen1W = 2560
screen1H = 1440

screen2W = 1024
screen2H = 768
