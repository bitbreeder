module Main(main) where

import Prelude hiding (init)

import Control.Exception (handle, SomeException)
import Control.Monad (forever, when)
import System.Environment (getArgs)
import System.IO (hSetBuffering, BufferMode(LineBuffering), stdin, stdout)

import Graphics.UI.GLFW

import Config (videoX, videoY, videoW, videoH)
import Expression
import Video

main :: IO ()
main = do
  hSetBuffering stdin LineBuffering
  hSetBuffering stdout LineBuffering
  args <- getArgs
  let record = "--record" `elem` args
  True <- init
  windowHint $ WindowHint'Resizable False
  windowHint $ WindowHint'Decorated False
  Just window <- createWindow videoW videoH "BitBreeder Expression" Nothing Nothing
  setWindowPos window videoX videoY
  makeContextCurrent (Just window)
  s <- setupGL
  draw s (I 0)
  handle ((\_ -> return ()) :: SomeException -> IO ()) $ forever $ do
    ne@(n, e) <- readLn :: IO (Double, E)
    print ne
    draw s e
    swapBuffers window
    when record $ captureToPNG (pngFilename (floor n))
    pollEvents
  destroyWindow window
  terminate
