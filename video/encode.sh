#!/bin/bash
SESSION="${1}"
if [ "x${SESSION}" = "x" ]
then
  exit
fi
time ./bitbreeder_video --record < "${SESSION}.out" > /dev/null
FRAMES="$(( $(ffprobe -v quiet -show_streams -i "${SESSION}.wav" -of json | grep duration_ts | sed 's|.*: \(.*\),.*|\1|g') / 1920 ))"
pushd "${SESSION}"
PREVFRAME="00000000.png"
for FRAME in $(seq "$(( FRAMES - 125 ))")
do
  THISFRAME="$(printf "%08d" "${FRAME}").png"
  if [ -f "${THISFRAME}" ]
  then
    PREVFRAME="${THISFRAME}"
  else
    ln -s "${PREVFRAME}" "${THISFRAME}"
  fi
done
for FRAME in $(seq "$(( FRAMES - 124 ))" "${FRAMES}")
do
  THISFRAME="$(printf "%08d" "${FRAME}").png"
  ln -s "00000000.png" "${THISFRAME}"
done
popd
ffmpeg -i "${SESSION}/%08d.png" -i "${SESSION}.wav" -shortest "${SESSION}.mkv"
