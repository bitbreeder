all: bitbreeder-audio bitbreeder-judge-dl bitbreeder-judge-jack
	mkdir -p o

clean:
	rm -f bitbreeder-audio bitbreeder-judge-dl bitbreeder-judge-jack

bitbreeder-audio: audio/audio.c include/bitbreeder.h
	gcc -std=c99 -Wall -pedantic -Wextra -Wno-unused-parameter -O3 -o bitbreeder-audio audio/audio.c `pkg-config --cflags --libs jack` -ldl -lm -Iinclude

bitbreeder-judge-dl: judge/judge.c include/bitbreeder.h
	gcc -std=c99 -Wall -pedantic -Wextra -O3 -ggdb -o bitbreeder-judge-dl judge/judge.c -lm -lfftw3f -ldl -Iinclude -DJUDGE_DL

bitbreeder-judge-jack: judge/judge.c include/bitbreeder.h
	gcc -std=c99 -Wall -pedantic -Wextra -O3 -ggdb -o bitbreeder-judge-jack judge/judge.c -lm -lfftw3f -ljack -Iinclude -DJUDGE_JACK

