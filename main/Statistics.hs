type R = Double

data Stat = Stat !R !R !R

Stat s0 s1 s2 <> Stat t0 t1 t2 = Stat (s0 + t0) (s1 + t1) (s2 + t2)

stat :: R -> Stat
stat x = Stat 1 x (x*x)

mean :: Stat -> R
mean (Stat s0 s1 _) = s1 / s0

stddev :: Stat -> R
stddev (Stat s0 s1 s2)
  | t > 0 = t
  | otherwise = 0
  where t = sqrt (s0 * s2 - s1 * s1) / s0

type Stats = (Stat, [Stat])

merge :: Stats -> Stats -> Stats
merge (s, ss) (t, ts) = (r, stat (mean r) : zipWith (<>) ss ts)
  where r = s <> t

pairwise f (a:b:cs) = f a b : pairwise f cs
pairwise _ _ = [ ]

granularity :: [R] -> R
granularity
  = sum . zipWith (*) [0..] . map stddev
  . reverse . snd . head . last . takeWhile (not . null)
  . iterate (pairwise merge) . map (\x -> (stat x, []))

go :: (R -> R) -> IO ()
go f = print $ granularity [ f t | t <- [1 .. 2^16] ]

main :: IO ()
main = do
  go (\t -> sin (t / 10))
  go (\t -> sin (t / 1000))
  go (\t -> sin (t / 10) + 10 * sin (t / 1000))
  go (\t -> sin (10 / (t + 1)))

{-
6.305600061502868
43.442727416334165
434.64767794388223
0.5956135757335486
-}
