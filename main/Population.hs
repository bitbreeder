module Population(Item(..), Population, empty, update, target, toAscList, sort) where

import Database (DB)
import qualified Database as D
import Metric (Stats, Analysis, Target, Weight)
import qualified Metric as S
import Expression (E())

data Item = Item{ itemID :: !Int, itemExpr :: E, itemMetric :: Analysis }

type Population = (DB Item, Stats, Target, Weight)

empty :: Population
empty = (D.empty, S.emptyStats, S.emptyTarget, S.emptyWeight)

insert :: Item -> Population -> Population
insert it (d, s, t, w) = (D.insert it d, S.insert (itemMetric it) s, t, w)

toAscList :: Population -> [Item]
toAscList (d, _, _, _) = D.toAscList d

prune :: Int -> Population -> (Population, [Int])
prune n p@(d, s, t, w) = case fmap D.toAscList $ D.splitAt n (D.sortOn (negate . fromIntegral . itemID) d) of
  (_, []) -> (p, [])
  (keep, discard) -> (sort (keep, foldr S.delete s $ map itemMetric discard, t, w), map itemID discard)
{-
   foldr S.insert S.emptyStats . map itemMetric . D.toAscList $ k, t, w)
  let (keep, discard) = D.splitAt n d
  in  (keep,  . D.toAscList $ discard, t, w)
-}

sort :: Population -> Population
sort (d, s, t, w) = (D.sortOn (S.score (S.target t w s) . itemMetric) d, s, t, w)

update :: Item -> Population -> (Population, [Int])
update it = prune maxPopCount . insert it

target :: Target -> Weight -> Population -> Population
target t w (d, s, _, _) = sort (d, s, t, w)

maxPopCount :: Int
maxPopCount = 1024
