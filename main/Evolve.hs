module Evolve (crossBreed) where

import Control.Monad (replicateM, forM)
import Control.Monad.Random (MonadRandom, getRandomR)

import Expression
import Genetics (nodes, exchange)

mutateI :: (Applicative m, MonadRandom m) => E -> m E
mutateI X = do
  k <- coin 0.1
  return (if k then Y else X)
mutateI Y = do
  k <- coin 0.1
  return (if k then X else Y)
mutateI (I i) = do
  k <- coin 0.1
  if k
    then do
      j <- getRandomR (1, 64)
      return (I j)
    else return (I i)
mutateI (U u e) = U u <$> mutateI e
mutateI (B b e f) = B b <$> mutateI e <*> mutateI f
mutateI (T e f g) = T <$> mutateI e <*> mutateI f <*> mutateI g

coin :: (Functor m, MonadRandom m) => Double -> m Bool
coin p = (< p) <$> getRandomR (0, 1)

terminal :: (Functor m, MonadRandom m) => m E
terminal = do
  c <- coin 0.5
  d <- coin 0.25
  if c then return (if d then Y else X) else I <$> getRandomR (1, 64)

data F
  = FU U
  | FB B
  | FT
  deriving (Read, Show, Eq)

getRandomE :: (Functor m, MonadRandom m, Enum e, Bounded e) => m e
getRandomE = self
  where
    self = do
      mi <- return minBound `asTypeOf` self
      ma <- return maxBound `asTypeOf` self
      toEnum <$> getRandomR (fromEnum mi, fromEnum ma)

function :: (Functor m, MonadRandom m) => m F
function = do
  c <- coin 0.05
  if c then FU <$> getRandomE else do
    d <- coin 0.05
    if d then return FT else FB <$> getRandomE

grow :: (Applicative m, MonadRandom m) => Int -> m E
grow 0 = terminal
grow d = do
  c <- coin 0.25
  if c then terminal else do
    f <- function
    case f of
      FU u -> U u <$> grow (d - 1)
      FB b -> B b <$> grow (d - 1) <*> grow (d - 1)
      FT   -> T   <$> grow (d - 1) <*> grow (d - 1) <*> grow (d - 1)

breed :: (Applicative m, MonadRandom m) => E -> E -> m [E]
breed e0 e1 = do
  n0 <- getRandomR (0, nodes e0 - 1)
  n1 <- getRandomR (0, nodes e1 - 1)
  let (f0, f1) = exchange e0 n0 e1 n1
  return [f0, f1]

choose :: (Applicative m, MonadRandom m) => Double -> [a] -> m a
choose p xs = go (cycle xs)
  where
    go (x:xs) = do
      c <- coin p
      if c then return x else go xs
    go [] = error "Evolve.choose: internal error"

crossBreed :: (Applicative m, MonadRandom m) => [[E]] -> m [E]
crossBreed ess = do
  ws <- replicateM 2 $ getRandomR (0, length ess - 1)
  ~[e0, e1] <- forM ws $ \w -> do
    let es = take minPopCount (ess !! w)
    if length es < minPopCount then grow 5 else do
      e <- choose 0.1 es
      mutateI e
  e2 <- grow 5
  (e2:) <$> breed e0 e1

minPopCount :: Int
minPopCount = 512
