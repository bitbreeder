module Main(main) where

import Control.Exception (bracket, finally, handle, SomeException)
import Control.Monad (forever, forM, forM_, replicateM, when)
import Control.Concurrent (forkIO, threadDelay)
import GHC.Conc (numCapabilities)
import Data.List (nubBy, partition, sortOn)
import Data.Maybe (listToMaybe)
import Text.Read (readMaybe)
import System.Directory (removeFile)
import System.IO (hSetBuffering, BufferMode(LineBuffering), hPutStrLn, stdout, stderr)

import qualified Data.Map.Strict as Map

import Data.Vector.Unboxed ((!), (//))
import qualified Data.Vector.Unboxed as V

import Control.Concurrent.STM

import System.Process

import Control.Monad.Random (evalRandIO, getRandomR)

import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T

import qualified Network.WebSockets as WS

import Expression
import Evolve
import Genetics (nodes)
import Compile
import Population
import qualified Metric as M

data Client = Client Int WS.Connection
instance Eq Client where
  Client a _ == Client b _ = a == b

data Server = Server
 { clients :: [Client]
 , populations :: [(Int, TVar Population)]
 }

addClient :: Client -> Server -> Server
addClient client state = state{ clients = client : clients state }

removeClient :: Client -> Server -> Server
removeClient client state = state{ clients = filter (client /=) (clients state) }

bitbreeder :: Int -> Int -> Int -> IO ()
bitbreeder populationCount breederCount judgeCount = do
  forkIO $ streamer
  threadDelay (10 * 1000 * 1000) -- wait for streamer to start
  lock <- newTMVarIO ()
  state <- newTVarIO $ Server{ clients = [], populations = [] }
  forM [0 .. populationCount - 1] $ newPopulation lock state
  toJudge <- newTBQueueIO (fromIntegral $ 2 * 3 * breederCount)
  toUpdate <- newTBQueueIO (fromIntegral $ 2 * judgeCount)
  forkIO $ updater populationCount state toUpdate
  forM_ [0 .. breederCount - 1] (forkIO . breeder state toJudge)
  forM_ [0 .. judgeCount - 1] (forkIO . judge toJudge toUpdate judgeCount)
  forkIO $ extinction state
  forkIO $ randomizer lock state populationCount
  clientID <- newTVarIO 0
  WS.runServer "0.0.0.0" 8002 $ server clientID lock state

server clientID lock state pending = do
  client <- WS.acceptRequest pending
  id <- atomically $ do
    id <- readTVar clientID
    writeTVar clientID $! id + 1
    return id
  let c = Client id client
  WS.withPingThread client 30 (return ()) $
    (do
       atomically $ modifyTVar state (addClient c)
       sendState c state
       talk lock c state
    ) `finally`
    (do
       atomically $ modifyTVar state (removeClient c)
    )

streamer :: IO ()
streamer = forever $ do
  ignoreErrors $
    callCommand $
      "darkice -c darkice.cfg"
  threadDelay (10 * 1000 * 1000)

soName :: Int -> String
soName i = "./o/" ++ show i ++ ".so"

newPopulation :: TMVar () -> TVar Server -> Int -> IO ()
newPopulation lock state popN = do
  (Just h, _, _, _) <- createProcess (proc "./bitbreeder-audio" [show popN]){ std_in = CreatePipe, std_err = Inherit }
  hSetBuffering h LineBuffering
  pop <- newTVarIO empty
  let toAudio n = do
        i <- atomically $ do
          mi <- listToMaybe . toAscList <$> readTVar pop
          case mi of
            Just i | itemID i /= n -> return i
            _ -> retry
        hPutStrLn h (soName (itemID i))
        broadcast lock Nothing state $ T.pack ("E " ++ show popN ++ " " ++ rpn (itemExpr i))
        toAudio (itemID i)
  forkIO $ toAudio (-1)
  atomically $ modifyTVar state (\s -> s{ populations = (popN, pop) : populations s })

extinction :: TVar Server -> IO ()
extinction state = forever $ do
  threadDelay (60 * 60 * 1000 * 1000)
  delete <- atomically $ do
    s <- readTVar state
    dss <- forM (populations s) $ \(_, pop) -> do
      p <- readTVar pop
      writeTVar pop empty
      return (map itemID $ toAscList p)
    return (concat dss)
  forM_ delete $ \i -> ignoreErrors $ removeFile (soName i)

breeder :: TVar Server -> TBQueue E -> Int -> IO ()
breeder state toJudge n = forever $ do
    ess <- atomically $ do
      s <- readTVar state
      map (map itemExpr . toAscList) <$> mapM (readTVar . snd) (populations s)
    es <- evalRandIO (crossBreed ess)
    forM_ es $ \e -> atomically $ writeTBQueue toJudge e

judge :: TBQueue E -> TBQueue Item -> Int -> Int -> IO ()
judge toJudge toUpdate count = loop
  where
    loop i = do
      let so = soName i
      e <- atomically $ readTBQueue toJudge
      ignoreErrors $ do
        handle ((\_ -> removeFile so) :: SomeException -> IO ()) $ do
          compileSO e so
          (_, Just hout, _, p) <- createProcess (proc "./bitbreeder-judge-dl" [so, show i, show (nodes e)]){ std_out = CreatePipe, std_err = Inherit }
          v@(M.A vv) <- M.read hout
          _ <- waitForProcess p
          if vv ! 0 > 0.001
            then atomically $ writeTBQueue toUpdate (Item i e v)
            else removeFile so
      loop (i + count)

updater :: Int -> TVar Server -> TBQueue Item -> IO ()
updater count state toUpdate = forever $ do
      q <- evalRandIO $ getRandomR (0, count - 1)
      delete <- atomically $ do
        item <- readTBQueue toUpdate
        s <- readTVar state
        case lookup q (populations s) of
          Just pop -> do
            p <- readTVar pop
            let (p', d) = update item p
            writeTVar pop p'
            return d
          _ -> retry
      forM_ delete $ removeFile . soName

talk :: TMVar () -> Client -> TVar Server -> IO ()
talk lock client@(Client _ conn) state = forever $ do
  message <- WS.receiveData conn
  case parse message of
    Nothing -> return ()
    Just m -> do
      ok <- process state m
      when ok $ broadcast lock (Just client) state (pretty m)

equating :: Eq c => (a -> c) -> a -> a -> Bool
equating f a b = f a == f b

randomizer :: TMVar () -> TVar Server -> Int -> IO ()
randomizer lock state populations = forever $ do
  ms <- fmap (nubBy (equating fst)) . evalRandIO $ replicateM (6 * populations) $ do
    pop <- getRandomR (0, populations - 1)
    param <- getRandomR (0, M.elements - 2 - 1) -- ignore novelty and size
    target <- getRandomR (-5, 5)
    weight <- getRandomR (0, 1)
    return ((pop, param), (target, weight))
  forM_ [0 .. populations - 1] $ \p -> forM_ [0 .. M.elements - 1] $ \e -> do
    let m = SetWeight p e 0
    ok <- process state m
    when ok $ broadcast lock Nothing state (pretty m)
  forM_ ms $ \((p, e), (t, w)) -> do
    let mt = SetTarget p e t
    okt <- process state mt
    when okt $ broadcast lock Nothing state (pretty mt)
    let mw = SetWeight p e w
    okw <- process state mw
    when okw $ broadcast lock Nothing state (pretty mw)
  threadDelay (10 * 60 * 1000 * 1000)

process :: TVar Server -> Message -> IO Bool
process state message = atomically $ do
    s <- readTVar state
    case lookup (setPopulation message) (populations s) of
      Just p -> case message of
          SetTarget{} -> if 0 <= setParameter message &&
                            setParameter message < M.elements &&
                            -5 <= setValue message &&
                            setValue message <= 5
                           then do
                             modifyTVar p $ \(db, stats, M.T target, M.W weight) -> Population.sort (db, stats, M.T $ target // [(setParameter message, setValue message)], M.W weight)
                             return True
                           else do
                             return False
          SetWeight{} -> if 0 <= setParameter message &&
                            setParameter message < M.elements &&
                            0 <= setValue message &&
                            setValue message <= 1
                           then do
                             modifyTVar p $ \(db, stats, M.T target, M.W weight) -> Population.sort (db, stats, M.T target, M.W $ weight // [(setParameter message, setValue message)])
                             return True
                           else
                             return False
      _ -> return False

sendState :: Client -> TVar Server -> IO ()
sendState client@(Client _ conn) state = do
  pops <- atomically $ do
    s <- readTVar state
    forM (populations s) $ \(n, p) -> do
      (,) n <$> readTVar p
  forM_ pops $ \(n, (_, _, M.T target, M.W weight)) -> do
    forM_ (zip [0..] (V.toList target)) $ \(p, v) -> do
      WS.sendTextData conn (pretty (SetTarget n p v))
    forM_ (zip [0..] (V.toList weight)) $ \(p, v) -> do
      WS.sendTextData conn (pretty (SetWeight n p v))

broadcast :: TMVar () -> Maybe Client -> TVar Server -> Text -> IO ()
broadcast lock client state message = bracket (atomically $ takeTMVar lock) (atomically . putTMVar lock) $ \() -> do
  T.hPutStrLn stderr message
  s <- atomically $ readTVar state
  forM_ (filter (client /=) (map Just (clients s))) $ \(Just (Client _ conn)) -> do
    WS.sendTextData conn message

data Message
  = SetTarget{ setPopulation :: !Int, setParameter :: !Int, setValue :: !Double }
  | SetWeight{ setPopulation :: !Int, setParameter :: !Int, setValue :: !Double }

pretty :: Message -> Text
pretty (SetTarget p s v) = T.pack (unwords ["T", show p, show s, show v])
pretty (SetWeight p s v) = T.pack (unwords ["W", show p, show s, show v])

parse :: Text -> Maybe Message
parse t = case words (T.unpack t) of
  ["T", sp, ss, sv] -> case (readMaybe sp, readMaybe ss, readMaybe sv) of
    (Just p, Just s, Just v) -> Just $ SetTarget p s v
    _ -> Nothing
  ["W", sp, ss, sv] -> case (readMaybe sp, readMaybe ss, readMaybe sv) of
    (Just p, Just s, Just v) -> Just $ SetWeight p s v
    _ -> Nothing
  _ -> Nothing

ignoreErrors :: IO () -> IO ()
ignoreErrors = handle ((\_ -> return ()) :: SomeException -> IO ())

main :: IO ()
main = bitbreeder 4 numCapabilities numCapabilities
