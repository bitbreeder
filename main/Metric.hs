module Metric where

import Prelude hiding (read, replicate, sum, zip, zip3, zipWith, zipWith3)

import Control.Monad (when)
import Data.Vector.Unboxed
import qualified Data.Vector.Storable as S (thaw, unsafeFreeze)
import qualified Data.Vector.Storable.Mutable as S (unsafeWith)

import Foreign (allocaBytes, copyBytes)
import System.IO (Handle, hGetBuf)
import System.IO.Error (mkIOError, eofErrorType)

newtype Metric   = M (Vector (Double, Double))
newtype Stats    = S (Vector (Double, Double, Double))
newtype Analysis = A (Vector Double)
newtype Weight   = W (Vector Double)
newtype Target   = T (Vector Double)
type    Score    = Double

emptyMetric   :: Metric
emptyMetric   =  M $ zip zero zero

emptyStats    :: Stats
emptyStats    =  S $ zip3 zero zero zero

emptyAnalysis :: Analysis
emptyAnalysis =  A zero

emptyWeight   :: Weight
emptyWeight   =  W zero

emptyTarget   :: Target
emptyTarget   =  T zero

zero :: Vector Double
zero = replicate elements 0

insert :: Analysis -> Stats -> Stats
insert (A as) (S ss) = S $ zipWith f as ss
  where
    f a (s0, s1, s2) = (s0 + 1, s1 + a, s2 + a * a)

delete :: Analysis -> Stats -> Stats
delete (A as) (S ss) = S $ zipWith f as ss
  where
    f a (s0, s1, s2) = (s0 - 1, s1 - a, s2 - a * a)

target :: Target -> Weight -> Stats -> Metric
target (T ts) (W ws) (S ss) = M $ zipWith3 f ws ts ss
  where
    f w t (s0, s1, s2)
      | isInfinite tt = (0, 0)
      | isNaN      tt = (0, 0)
      | isInfinite ww = (0, 0)
      | isNaN      ww = (0, 0)
      | otherwise     = (tt, ww)
      where
        mean = s1 / s0
        stddev = sqrt (s0 * s2 - s1 * s1) / s0
        tt = mean + stddev * t
        ww = w / stddev

score :: Metric -> Analysis -> Score
score (M ms) = \(A as) -> sum $ zipWith f ms as
  where
    f (t, w) = \a -> let d = w * (a - t) in d * d

read :: Handle -> IO Analysis
read h = allocaBytes bytes $ \ptr -> do
  bytes' <- hGetBuf h ptr bytes
  when (bytes /= bytes') $ ioError (mkIOError eofErrorType "" Nothing Nothing)
  m <- S.thaw (convert zero)
  S.unsafeWith m $ \q -> copyBytes q ptr bytes
  (A . convert) `fmap` S.unsafeFreeze m

bytes, elements, measurements, parameters :: Int
bytes = elements * 8
elements = measurements * parameters + 2
measurements = 6
parameters = 3
