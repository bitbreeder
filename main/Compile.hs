module Compile (compile, compileSO) where

import System.Process (rawSystem)

import Expression

compile :: E -> String
compile X = "t"
compile Y = "c"
compile (I i) = show i
compile (U u e) = compileU u ("(" ++ compile e ++ ")")
compile (B b e f) = compileB b ("(" ++ compile e ++ ")") ("(" ++ compile f ++ ")")
compile (T e f g) = "(" ++ compile e ++ ")?(" ++ compile f ++ "):(" ++ compile g ++ ")"

compileU :: U -> String -> String
compileU Neg s = "-" ++ s
compileU LNot s = "-(!" ++ s ++ ")"
compileU BNot s = "~" ++ s

compileB :: B -> String -> String -> String
compileB Add = fn "safe_add"
compileB Sub = fn "safe_sub"
compileB Mul = fn "safe_mul"
compileB Div = fn "safe_div"
compileB Mod = fn "safe_mod"
compileB BAnd = op "&"
compileB LAnd = \l r -> "-((int)(" ++ op "&&" l r ++ "))"
compileB BOr = op "|"
compileB LOr = \l r -> "-((int)(" ++ op "||" l r ++ "))"
compileB XOr = op "^"
compileB ShL = fn "safe_shl"
compileB ShR = fn "safe_shr"
compileB Lt  = \l r -> "-((int)(" ++ op "<" l r ++ "))"
compileB Gt  = \l r -> "-((int)(" ++ op ">" l r ++ "))"

op, fn :: String -> String -> String -> String
op o a b = a ++ o ++ b
fn f a b = f ++ "(" ++ a ++ "," ++ b ++ ")"

compileSO :: E -> FilePath -> IO ()
compileSO e so = do
  let code = compile e
  _ <- rawSystem "gcc"
          [ "-std=c99", "-Wall", "-Wextra", "-pedantic", "-Wno-unused-parameter", "-Wno-overlength-strings", "-O3", "-shared", "-fPIC", "include/bitbreeder.c"
          , "-o", so, "-DT=" ++ code, "-DCODE=\"" ++ code ++ "\""
          ]
  return ()
