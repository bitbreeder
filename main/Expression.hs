{-# LANGUAGE DeriveDataTypeable #-}
module Expression where

import Data.Data (Data)
import Data.Typeable (Typeable)

data E = X | Y | I Int | U U E | B B E E | T E E E
  deriving (Read, Show, Eq, Ord, Data, Typeable)

data U = Neg | LNot | BNot
  deriving (Read, Show, Eq, Ord, Enum, Bounded, Data, Typeable)

data B = Add | Sub | Mul | Div | Mod | BAnd | LAnd | BOr | LOr | XOr | ShL | ShR | Lt | Gt
  deriving (Read, Show, Eq, Ord, Enum, Bounded, Data, Typeable)

count :: Integer -> Integer
count 0 = 0
count 1 = 65
count n = 3 * count (n - 1) + sum [ 14 * (count l + count r) | l <- [1 .. n - 2], let r = n - 1 - l ]

rpn :: E -> String
rpn X = "t"
rpn Y = "c"
rpn (I n) = show n
rpn (U u e) = rpn e ++ " " ++ rpnU u
rpn (B b e f) = rpn e ++ " " ++ rpn f ++ " " ++ rpnB b
rpn (T e f g) = rpn f ++ " " ++ rpn g ++ " " ++ rpn e ++ " " ++ "?:"

rpnU :: U -> String
rpnU Neg = "neg"
rpnU LNot = "!"
rpnU BNot = "~"

rpnB :: B -> String
rpnB Add = "+"
rpnB Sub = "-"
rpnB Mul = "*"
rpnB Div = "/"
rpnB Mod = "%"
rpnB BAnd = "&"
rpnB LAnd = "&&"
rpnB BOr = "|"
rpnB LOr = "||"
rpnB XOr = "^"
rpnB ShL = "<<"
rpnB ShR = ">>"
rpnB Lt = "<"
rpnB Gt = ">"
