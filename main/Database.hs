module Database(DB(), empty, insert, sortOn, toAscList, splitAt, fromList) where

import Prelude hiding (splitAt)

import Data.List (insertBy, sortBy)
import qualified Data.List as L (splitAt)
import Data.Ord (comparing)

data DB a = DB
  { _insert    :: a -> DB a
  , _sortOn    :: (a -> Double) -> DB a
  , _toAscList :: [a]
  , _splitAt :: Int -> (DB a, DB a)
  }

empty :: DB a
empty = mkDB (const 0) []

insert :: a -> DB a -> DB a
insert = flip _insert

sortOn :: (a -> Double) -> DB a -> DB a
sortOn = flip _sortOn

toAscList :: DB a -> [a]
toAscList = _toAscList

splitAt :: Int -> DB a -> (DB a, DB a)
splitAt = flip _splitAt

fromList :: [a] -> DB a
fromList = foldr insert empty

-- invariants
--  list == L.sortBy (comparing snd) list
--  all [metric x == y | (x, y) <- list]
mkDB :: (a -> Double) -> [(a, Double)] -> DB a
mkDB metric list = DB
  { _insert = \item    -> mkDB metric  (insertBy (comparing snd) (item, metric item) list)
  , _sortOn = \metric' -> mkDB metric' (sortBy   (comparing snd) (map (\(x, _) -> (x, metric' x)) list))
  , _toAscList = map fst list
  , _splitAt = \n -> let (lo, hi) = L.splitAt n list in (mkDB metric lo, mkDB metric hi)
  }
