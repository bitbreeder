#define _GNU_SOURCE
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <dlfcn.h>

#include <jack/jack.h>

#include "bitbreeder.h"

typedef float callback(int, int);

// default silent callback
static float deffunc(int t, int c) {
  return 0;
}

static struct {
  jack_client_t *client;
  jack_port_t *out[2];
  int k, t, swap_channels;
  callback * volatile func;
  float a[4][2];
  float dc[2];
} state;

// race mitigation
volatile int inprocesscb = 0;

static int processcb(jack_nframes_t nframes, void *arg) {
  inprocesscb = 1; // race mitigation
  jack_default_audio_sample_t *out[2] =
    { (jack_default_audio_sample_t *) jack_port_get_buffer(state.out[0], nframes)
    , (jack_default_audio_sample_t *) jack_port_get_buffer(state.out[1], nframes)
    };
  for (jack_nframes_t i = 0; i < nframes; ++i) {
    out[0][i] = 0;
    out[1][i] = 0;
  }
  callback *f = state.func;
  for (jack_nframes_t i = 0; i < nframes; ++i) {
    int k = (++state.k % 5);
    // 48000/5 ~ 140bpm, 48000/6 ~ 117bpm
    if (k == 0/*6*/) {
      state.k = 0;
      for (int c = 0; c < 2; ++c)
      {
        float fval = f(state.t, c ^ state.swap_channels);
        if (isnan(fval) || isinf(fval)) fval = 0;
        state.a[0][c] = state.a[1][c];
        state.a[1][c] = state.a[2][c];
        state.a[2][c] = state.a[3][c];
        state.a[3][c] = fval;
      }
      ++state.t;
    }
    /*
      -- http://en.wikipedia.org/wiki/Cubic_Hermite_spline#Interpolation_on_the_unit_interval_without_exact_derivatives
      putStr . unlines . map (show . map (/2)) $ [
        [ -x^3 + 2*x^2 - x
        , 3 * x^3 - 5 * x^2 + 2
        , -3 * x^3 + 4 * x^2 + x
        , x^3 - x^2
        ] | i <- [0..5], let x = i / 6 ]
    */
  /*
    const float p[6][4] =
      { {0.0,1.0,0.0,0.0}
      , {-5.7870370370370364e-2,0.9375,0.13194444444444442,-1.1574074074074073e-2}
      , {-7.407407407407407e-2,0.7777777777777778,0.3333333333333333,-3.7037037037037035e-2}
      , {-6.25e-2,0.5625,0.5625,-6.25e-2}
      , {-3.7037037037037035e-2,0.33333333333333326,0.7777777777777777,-7.407407407407407e-2}
      , {-1.157407407407407e-2,0.13194444444444442,0.9375,-5.787037037037035e-2}
      };
  */
    const float p[5][4] =
      { {0.0,1.0,0.0,0.0}
      , {-0.064,0.912,0.168,-0.016}
      , {-0.072,0.696,0.424,-0.048}
      , {-0.048,0.424,0.696,-0.072}
      , {-0.016,0.168,0.912,-0.064}
      };
    float a[2];
    for (int c = 0; c < 2; ++c)
    {
      a[c] = p[k][0] * state.a[0][c] + p[k][1] * state.a[1][c] + p[k][2] * state.a[2][c] + p[k][3] * state.a[3][c];
      if (isnan(a[c]) || isinf(a[c]))
      {
        a[c] = 0;
      }
      state.dc[c] = state.dc[c] * 0.99 + 0.01 * a[c];
      if (isnan(state.dc[c]) || isinf(state.dc[c]))
      {
        state.dc[c] = 0;
      }
      a[c] -= state.dc[c];
      if (isnan(a[c]) || isinf(a[c]))
      {
        a[c] = 0;
      }
    }
    out[0][i] = a[0];
    out[1][i] = a[1];
  }
  inprocesscb = 0; // race mitigation
  return 0;
}

static void errorcb(const char *desc) {
  fprintf(stderr, "JACK error: %s\n", desc);
}

static void shutdowncb(void *arg) {
  exit(1);
}

static void atexitcb(void) {
  jack_client_close(state.client);
}

int main(int argc, char **argv) {
  srand(time(0));
  state.func = deffunc;
  jack_set_error_function(errorcb);
  int client_number = argc > 1 ? atoi(argv[1]) : 0;
  state.swap_channels = client_number & 1;
  char client_name[32];
  snprintf(client_name, sizeof(client_name), "bitbreeder-%d", client_number);
  if (!(state.client = jack_client_open(client_name, JackNoStartServer, 0))) {
    fprintf(stderr, "jack server not running?\n");
    return 1;
  }
  atexit(atexitcb);
  jack_set_process_callback(state.client, processcb, 0);
  jack_on_shutdown(state.client, shutdowncb, 0);

  // stereo processing
  state.out[0] = jack_port_register(state.client, "output_1", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
  state.out[1] = jack_port_register(state.client, "output_2", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
  state.k = 0;
  state.t = time(0) * (48000 / 5);
  if (jack_activate(state.client)) {
    fprintf (stderr, "cannot activate JACK client");
    return 1;
  }

  // stereo streaming
  int stream_connected = 1;
  if (jack_connect(state.client, jack_port_name(state.out[0]), "bitbreeder-stream:left")) {
    stream_connected = 0;
  }
  if (jack_connect(state.client, jack_port_name(state.out[1]), "bitbreeder-stream:right")) {
    stream_connected = 0;
  }
  if (stream_connected)
  {
    fprintf(stderr, "connected to stream\n");
  }
  else
  {
    fprintf(stderr, "could not connect to stream\n");
  }

  // stereo output
  if (! stream_connected)
  {
    const char **ports;
    if ((ports = jack_get_ports(state.client, NULL, NULL, JackPortIsPhysical | JackPortIsInput))) {
      int i = 0;
      while (ports[i] && i < 2) {
        if (jack_connect(state.client, jack_port_name(state.out[i]), ports[i])) {
          fprintf(stderr, "cannot connect output port %d\n", i);
        }
        i++;
      }
      free(ports);
    }
  }

  void *old_dl = 0;
  void *new_dl = 0;
  char *soname = 0;
  while (1 == scanf("%ms", &soname)) {
    if ((new_dl = dlopen(soname, RTLD_NOW))) {
      callback *new_cb;
      *(void **) (&new_cb) = dlsym(new_dl, "F");
      if (new_cb) {
        // race mitigation: dlclose with jack running in .so -> boom
        while (inprocesscb) ;
        state.func = new_cb;
        if (old_dl) {
          while (inprocesscb) ;
          dlclose(old_dl);
        }
        old_dl = new_dl;
      } else {
        dlclose(new_dl);
        new_dl = 0;
      }
    } else {
    }
    free(soname);
    soname = 0;
  }
  return 0;
}
