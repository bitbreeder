module Main (main) where

import Control.Concurrent (forkIO)
import Control.Monad (forever)

import qualified Data.Text.IO as T
import qualified Network.WebSockets as WS

main = WS.runClient "127.0.0.1" 8002 "" $ \ws -> do
  _ <- forkIO $ forever $ T.putStrLn =<< WS.receiveData ws
  forever $ WS.sendTextData ws =<< T.getLine
